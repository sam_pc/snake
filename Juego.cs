﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace Snake
{
    class Juego
    {
        private Point lugarComida = Point.Empty;
        private Serpiente serpiente = new Serpiente();
        private int marcador = 0;

       
        private void MostarPunteo(int punteo)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(1, 0);
            Console.Write($"Puntuación: {punteo.ToString("00000000")}");
        }

        

        private Point MostrarComida(Size tamañoPantalla)
        {
            Point colaSerpiente = Point.Empty;
            Point cabezaSerpiente = serpiente.Cola.Last();
            Random random = new Random();
            do
            {
                int x = random.Next(0, tamañoPantalla.Width - 1);
                int y = random.Next(0, tamañoPantalla.Height - 1);
                if (serpiente.Cola.All(p => p.X != x || p.Y != y)
                    && Math.Abs(x - cabezaSerpiente.X) + Math.Abs(y - cabezaSerpiente.Y) > 8)
                {
                    colaSerpiente = new Point(x, y);
                }

            } while (colaSerpiente == Point.Empty);

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.SetCursorPosition(colaSerpiente.X + 1, colaSerpiente.Y + 1);
            Console.Write(" ");

            return colaSerpiente;
        }

        private void EjecutarJuego(Size tamañoPantalla)
        {
            serpiente.Cola.Enqueue(serpiente.Posicion);

            while (serpiente.MueveSerpiente(tamañoPantalla))
            {
                Thread.Sleep(serpiente.Velocidad);
                serpiente.Direccion = serpiente.ObtieneDireccion();
                serpiente.Posicion = serpiente.ObtieneSiguientePosicion();

                if (serpiente.Posicion.Equals(lugarComida))
                {
                    lugarComida = Point.Empty;
                    serpiente.Tamaño++;
                    MostarPunteo(marcador++);
                }

                // no hay comida en pantalla para mostrar, por lo que la muestra
                if (lugarComida == Point.Empty)
                {
                    lugarComida = MostrarComida(tamañoPantalla);
                }
            }

            Console.ResetColor();
            Console.SetCursorPosition(tamañoPantalla.Width / 2 - 4, tamañoPantalla.Height / 2);
            Console.Write("Gracias por jugar");
            Console.Beep();
            Thread.Sleep(2000);
            Console.ReadKey();
        }

        private void DibujarPantalla(Size size)
        {
            Console.Title = "Snake";
            Console.WindowHeight = size.Height + 2;
            Console.WindowWidth = size.Width + 2;
            Console.BufferHeight = Console.WindowHeight;
            Console.BufferWidth = Console.WindowWidth;
            Console.CursorVisible = false;
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Black;
            for (int fila = 0; fila < size.Height; fila++)
            {
                for (int columna = 0; columna < size.Width; columna++)
                {
                    // pinta la pantalla
                    Console.SetCursorPosition(columna + 1, fila + 1);
                    Console.Write(" ");
                }
            }
        }

        public void Iniciador()
        {
            Size TamañoPantalla = new Size(60, 20);

            DibujarPantalla(TamañoPantalla);
            MostarPunteo(marcador);
            EjecutarJuego(TamañoPantalla);

        }

        // constructor
        static void Main() { new Juego().Iniciador(); }

    } 

    internal enum Direction
    {
        Down, Left, Right, Up
    }
}
