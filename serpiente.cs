﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    class Serpiente
    {
        //atributos
        private Queue<Point> cola;
        private Point posicion;
        private Direction direccion;
        private int tamaño, velocidad;

        //getters y setters
        public Queue<Point> Cola { get => cola;}
        public Point Posicion { get => posicion; set => posicion = value; }
        public Direction Direccion { get => direccion; set => direccion = value; }
        public int Tamaño { get => tamaño; set => tamaño = value; }
        public int Velocidad { get => velocidad; set => velocidad = value; }

        //metodos
        public Direction ObtieneDireccion()
        {
            if (!Console.KeyAvailable) return direccion;

            var tecla = Console.ReadKey(true).Key;
            switch (tecla)
            {
                case ConsoleKey.DownArrow:
                    if (direccion != Direction.Up)
                        direccion = Direction.Down;
                    break;
                case ConsoleKey.LeftArrow:
                    if (direccion != Direction.Right)
                        direccion = Direction.Left;
                    break;
                case ConsoleKey.RightArrow:
                    if (direccion != Direction.Left)
                        direccion = Direction.Right;
                    break;
                case ConsoleKey.UpArrow:
                    if (direccion != Direction.Down)
                        direccion = Direction.Up;
                    break;
            }
            return direccion;
        }
        public Point ObtieneSiguientePosicion()
        {
            Point posicionSiguiente = new Point(posicion.X, posicion.Y);
            switch (Direccion)
            {
                case Direction.Up:
                    posicionSiguiente.Y--;
                    break;
                case Direction.Left:
                    posicionSiguiente.X--;
                    break;
                case Direction.Down:
                    posicionSiguiente.Y++;
                    break;
                case Direction.Right:
                    posicionSiguiente.X++;
                    break;
            }
            return posicionSiguiente;
        }
        public bool MueveSerpiente(Size tamañoPantalla)
        {

            // si retorna false es fin del juego
            // obtiene la última posición de la culebra (cabeza)
            Point ultimoPunto = cola.Last();

            if (ultimoPunto.Equals(Posicion)) return true;

            // evalua si choca consigo misma
            if (cola.Any(x => x.Equals(Posicion)))
            {
                return false;
            }
            if (Posicion.X < 0 || Posicion.X >= tamañoPantalla.Width ||
                Posicion.Y < 0 || Posicion.Y >= tamañoPantalla.Height)
            {
                return false;
            }

            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.SetCursorPosition(ultimoPunto.X + 1, ultimoPunto.Y + 1);
            Console.WriteLine(" ");

            cola.Enqueue(posicion);

            Console.BackgroundColor = ConsoleColor.Cyan;
            Console.SetCursorPosition(posicion.X + 1, posicion.Y + 1);
            Console.Write(" ");

            // Quitar cola
            if (cola.Count > tamaño )
            {
                Point puntoRemovido = cola.Dequeue();
                Console.BackgroundColor = ConsoleColor.Black;
                Console.SetCursorPosition(puntoRemovido.X + 1, puntoRemovido.Y + 1);
                Console.Write(" ");
            }
            return true;
        }

        //constructor
        public Serpiente()
        {
            this.cola = new Queue<Point>();
            this.posicion = new Point(0, 9);
            this.direccion = Direction.Right;
            this.tamaño = 6;
            this.velocidad = 100;
        }
    }
}
